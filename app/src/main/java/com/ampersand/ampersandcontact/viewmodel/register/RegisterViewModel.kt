package com.ampersand.ampersandcontact.viewmodel.register

import android.graphics.Bitmap
import android.telephony.PhoneNumberUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.model.ApiError
import com.ampersand.ampersandcontact.model.Register
import com.ampersand.ampersandcontact.repository.UserRepository
import com.ampersand.ampersandcontact.utils.LOG_KEY
import com.ampersand.ampersandcontact.viewmodel.UserResult
import io.reactivex.disposables.CompositeDisposable
import java.io.ByteArrayOutputStream
import java.util.regex.Pattern

class RegisterViewModel(
    private val userRepository: UserRepository
) : ViewModel() {

    private val registerObservable = MutableLiveData<RegisterFormState>()
    val registerFormState: LiveData<RegisterFormState> = registerObservable

    private val registerResultObservable = MutableLiveData<UserResult>()
    val userResultState: LiveData<UserResult> = registerResultObservable

    private val disposables = CompositeDisposable()


    fun registerUser(data: RegisterData) {
        disposables.add(
            userRepository
                .registerNewUser(
                    getRegisterUser(data),
                    { registerResult ->
                        registerResultObservable.value = UserResult(
                            success = registerResult.data
                        )
                    },
                    { registerError ->
                        registerResultObservable.value = UserResult(
                            apiError = registerError.apiError
                        )
                        Log.e(LOG_KEY, registerError.toString())
                    }
                )
        )
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun dataChanged(data: RegisterData) {
        val formState = RegisterFormState()

        formState.profilePhoto = registerFormState.value?.profilePhoto


        if (!isValidFullName(data.fullName)) {
            formState.fullNameError = R.string.full_name_validation
        } else if (!isValidEmailAddress(data.email)) {
            formState.emailError = R.string.email_validation
        } else if (!isValidPasswordLength(data.password)) {
            formState.passwordError = R.string.password_validation
        } else if (!isValidPhoneNumber(data.phoneNumber)) {
            formState.phoneNumberError = R.string.phone_validation
        } else if (!isValidTwitterHandle(data.twitter)) {
            formState.twitterError = R.string.twitter_validation
        } else if (!isValidLinkedIn(data.linkedIn)) {
            formState.linkedInErrror = R.string.linkedin_validation
        } else if (!isValidWebsite(data.website)) {
            formState.websiteError = R.string.phone_validation
        } else {
            formState.isDataValid = true
        }

        registerObservable.value = formState
    }

    fun updateProfilePhotoData(image: Bitmap?) {
        var formState = registerFormState.value
        if (formState == null) {
            formState = RegisterFormState()
        }

        formState.profilePhoto = image
        registerObservable.value = formState
    }

    private fun getRegisterUser(data: RegisterData): Register {
        val register = Register()

        val splat = data.fullName.split(' ')

        for (i in 0..splat.lastIndex) {
            if (i == 0) {
                register.firstName = splat[0]
            } else {
                register.lastName += "${splat[i]} "
            }
        }

        register.firstName.capitalize()
        register.lastName.trimEnd().capitalize()

        register.email = data.email
        register.password = data.password

        register.phoneNumber = data.phoneNumber
        register.twitter = data.twitter
        register.linkedIn = "https://linkedin.com${data.linkedIn}"
        register.website = data.website

        val photo64 = getBase64String()
        if (photo64 != null) {
            register.photo = "data:image/png;base64,$photo64"
        }

        println("base64 ==> ${register.photo}")

        return register
    }

    private fun getBase64String(): String? {
        val profilePhotoBitmap = registerFormState.value?.profilePhoto ?: return null

        val baos = ByteArrayOutputStream()
        profilePhotoBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b = baos.toByteArray()
        baos.close()

        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun isValidEmailAddress(email: String): Boolean =
        Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun isValidPasswordLength(password: String): Boolean =
        password.length >= 6

    private fun isValidFullName(fullName: String): Boolean {
        val regex = "^[\\p{L} .'-]+$"
        val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
        return pattern.matcher(fullName).matches()
    }

    private fun isValidPhoneNumber(phoneNumber: String): Boolean = PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)

    private fun isValidTwitterHandle(handle: String): Boolean = true

    private fun isValidLinkedIn(linkedInLink: String): Boolean = true

    private fun isValidWebsite(website: String): Boolean = true

    /*called only when request is success*/
    fun saveUser() {
        val userToken = userResultState.value?.success
        userRepository.saveCurrentUser(userToken!!)
    }
}