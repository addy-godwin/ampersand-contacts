package com.ampersand.ampersandcontact.viewmodel.signin

data class SignInFormState (
    var emailError: Int? = null,
    var passwordError: Int? = null,
    var isDataValid: Boolean = false
)