package com.ampersand.ampersandcontact.viewmodel.signin

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ampersand.ampersandcontact.repository.UserRepository

class SignInViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignInViewModel::class.java)) {
            return SignInViewModel(
                UserRepository(context)
            ) as T
        }
        throw IllegalArgumentException("View model class should be a subclass of RegisterViewModel")
    }

}