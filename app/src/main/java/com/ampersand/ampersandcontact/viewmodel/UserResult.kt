package com.ampersand.ampersandcontact.viewmodel

import com.ampersand.ampersandcontact.model.ApiError
import com.ampersand.ampersandcontact.model.UserToken

data class UserResult(
    var success: UserToken? = null,
    var apiError: ApiError? = null
)