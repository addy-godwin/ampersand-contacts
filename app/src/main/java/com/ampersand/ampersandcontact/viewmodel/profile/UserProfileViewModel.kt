package com.ampersand.ampersandcontact.viewmodel.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ampersand.ampersandcontact.model.UserToken
import com.ampersand.ampersandcontact.repository.UserRepository
import io.reactivex.disposables.CompositeDisposable

class UserProfileViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val userObservable = MutableLiveData<UserProfileResult>()
    val userState: LiveData<UserProfileResult> = userObservable

    private val userTokenObservable = MutableLiveData<UserToken>()
    val userTokenState: LiveData<UserToken> = userTokenObservable

    private val disposables = CompositeDisposable()

    init {
        userTokenObservable.value = userRepository.getCurrentUser()
    }

    fun findUser(userId: String, token: String) {
        disposables.add(
            userRepository.getUserProfile(userId, token,
                { profileResult ->
                    userObservable.value = UserProfileResult(user = profileResult.data)
                }, { errorResult ->
                    userObservable.value = UserProfileResult(
                        apiError = errorResult.apiError
                    )
                })
        )
    }
}