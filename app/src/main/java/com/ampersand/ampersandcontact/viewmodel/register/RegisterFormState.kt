package com.ampersand.ampersandcontact.viewmodel.register

import android.graphics.Bitmap

data class RegisterFormState(
    var profilePhoto: Bitmap? = null,
    var emailError: Int? = null,
    var fullNameError: Int? = null,
    var passwordError: Int? = null,
    var phoneNumberError: Int? = null,
    var twitterError: Int? = null,
    var linkedInErrror: Int? = null,
    var websiteError: Int? = null,
    var isDataValid: Boolean = false
)