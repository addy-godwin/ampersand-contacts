package com.ampersand.ampersandcontact.viewmodel.signin

data class SignInData(
    val email: String = "",
    val password: String = ""
)