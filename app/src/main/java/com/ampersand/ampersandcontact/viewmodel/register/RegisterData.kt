package com.ampersand.ampersandcontact.viewmodel.register

data class RegisterData(
    val email: String,
    val fullName: String,
    val password: String,
    val phoneNumber: String,
    val twitter: String,
    val linkedIn: String,
    val website: String
)