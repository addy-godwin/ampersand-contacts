package com.ampersand.ampersandcontact.viewmodel.usertoken

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ampersand.ampersandcontact.model.UserToken
import com.ampersand.ampersandcontact.repository.UserRepository

class UserTokenViewModel(userRepository: UserRepository) : ViewModel() {

    private val userTokenObservable = MutableLiveData<UserToken>()
    val userTokenState: LiveData<UserToken> = userTokenObservable


    init {
        userTokenObservable.value = userRepository.getCurrentUser()
    }
}