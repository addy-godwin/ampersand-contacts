package com.ampersand.ampersandcontact.viewmodel.signin

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.model.SignIn
import com.ampersand.ampersandcontact.model.UserToken
import com.ampersand.ampersandcontact.repository.UserRepository
import com.ampersand.ampersandcontact.viewmodel.UserResult
import io.reactivex.disposables.CompositeDisposable

class SignInViewModel(private val userRepository: UserRepository) : ViewModel() {
    private val signInObservable = MutableLiveData<SignInFormState>()
    val sigInFormState: LiveData<SignInFormState> = signInObservable

    private val sigInResultObservable = MutableLiveData<UserResult>()
    val userResultState: LiveData<UserResult> = sigInResultObservable

    private val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun signInUser(data: SignInData) {
        disposables.add(
            userRepository.signInUser(
                getSignInUser(data),
                { signInResult ->
                    sigInResultObservable.value = UserResult(success = signInResult.data)
                },
                { signInError ->
                    sigInResultObservable.value = UserResult(
                        apiError = signInError.apiError
                    )
                }
            )
        )
    }

    fun dataChanged(data: SignInData) {
        val formState = SignInFormState()

        if (!isValidEmailAddress(data.email)) {
            formState.emailError = R.string.email_validation
        } else if (!isValidPasswordLength(data.password)) {
            formState.passwordError = R.string.password_validation
        } else {
            formState.isDataValid = true
        }

        signInObservable.value = formState
    }

    private fun getSignInUser(data: SignInData): SignIn {
        return SignIn(
            email = data.email,
            password = data.password
        )
    }

    private fun isValidEmailAddress(email: String): Boolean =
        Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun isValidPasswordLength(password: String): Boolean =
        password.length >= 6

    fun saveUser(userToken: UserToken) {
        userRepository.saveCurrentUser(userToken)
    }
}