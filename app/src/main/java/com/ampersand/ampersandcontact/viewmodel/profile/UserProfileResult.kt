package com.ampersand.ampersandcontact.viewmodel.profile

import com.ampersand.ampersandcontact.model.ApiError
import com.ampersand.ampersandcontact.model.User

data class UserProfileResult(
    val user: User? = null,
    val apiError: ApiError? = null
)