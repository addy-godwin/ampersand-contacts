package com.ampersand.ampersandcontact.utils

import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.text.Editable
import android.text.TextWatcher
import com.ampersand.ampersandcontact.model.UserToken
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder


const val USER_KEY = "user_key"
const val USER_FILE_NAME = "user_file_name"

fun isOnline(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

fun saveUser(context: Context, userToken: UserToken) =
    saveObjectToSharedPreference(context, USER_FILE_NAME, USER_KEY, userToken)


fun getSavedUser(context: Context): UserToken? =
    getSavedObjectFromPreference(context, USER_FILE_NAME, USER_KEY, UserToken::class.java)

private fun saveObjectToSharedPreference(
    context: Context,
    preferenceFileName: String,
    serializedObjectKey: String,
    savedObject: Any
) {
    val sharedPreferences = context.getSharedPreferences(preferenceFileName, 0)
    val sharedPreferencesEditor = sharedPreferences.edit()
    val gson = Gson()
    val serializedObject = gson.toJson(savedObject)
    sharedPreferencesEditor.putString(serializedObjectKey, serializedObject)
    sharedPreferencesEditor.apply()
}

private fun <T> getSavedObjectFromPreference(
    context: Context,
    preferenceFileName: String,
    preferenceKey: String,
    classType: Class<T>
): T? {

    val sharedPreferences = context.getSharedPreferences(preferenceFileName, 0)

    if (sharedPreferences.contains(preferenceKey)) {
        val gson = Gson()
        return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
    }
    return null
}


fun makeQRCode(textToQR: String, width: Int = 300, height: Int = 300): Bitmap? {
    val multiFormatWriter = MultiFormatWriter()
    return try {
        val bitMatrix = multiFormatWriter.encode(
            textToQR,
            BarcodeFormat.QR_CODE,
            width,
            height
        )
        val barcodeEncoder = BarcodeEncoder()
        barcodeEncoder.createBitmap(bitMatrix)

    } catch (e: WriterException) {
        e.printStackTrace()
        null
    }
}


fun TextInputEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

