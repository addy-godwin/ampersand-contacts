package com.ampersand.ampersandcontact.utils

//api network values
const val TIMEOUT_IN_SECONDS = 20L
const val GALLERY_REQUEST_CODE = 100
const val CAMERA_REQUEST_CODE = 200

const val LOG_KEY = "ampersand contacts"

const val EXTRA_QR_USER_ID = "com.ampersand.id"