package com.ampersand.ampersandcontact.model


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("createdAt")
    var createdAt: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("firstName")
    var firstName: String,
    @SerializedName("_id")
    var id: String,
    @SerializedName("lastName")
    var lastName: String,
    @SerializedName("linkedIn")
    var linkedIn: String,
    @SerializedName("phoneNumber")
    var phoneNumber: String,
    @SerializedName("photo")
    var photo: String,
    @SerializedName("twitter")
    var twitter: String,
    @SerializedName("updatedAt")
    var updatedAt: String,
    @SerializedName("website")
    var website: String
)