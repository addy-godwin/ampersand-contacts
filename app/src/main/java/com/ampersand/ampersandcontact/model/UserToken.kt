package com.ampersand.ampersandcontact.model


import com.google.gson.annotations.SerializedName

data class UserToken(
    @SerializedName("token")
    var token: String,
    @SerializedName("user")
    var user: User
)