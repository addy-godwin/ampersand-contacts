package com.ampersand.ampersandcontact.model


import com.google.gson.annotations.SerializedName

data class ApiError(
    @SerializedName("error")
    var error: String = "",
    var code: Int = 0
)