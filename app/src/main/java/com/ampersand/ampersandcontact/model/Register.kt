package com.ampersand.ampersandcontact.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Register {

    @SerializedName("email")
    @Expose
    var email: String = ""
    @SerializedName("password")
    @Expose
    var password: String = ""
    @SerializedName("firstName")
    @Expose
    var firstName: String = ""
    @SerializedName("lastName")
    @Expose
    var lastName: String = ""
    @SerializedName("photo")
    @Expose
    var photo: String? = null
    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber: String = ""
    @SerializedName("twitter")
    @Expose
    var twitter: String = ""
    @SerializedName("linkedIn")
    @Expose
    var linkedIn: String = ""
    @SerializedName("website")
    @Expose
    var website: String = ""
}
