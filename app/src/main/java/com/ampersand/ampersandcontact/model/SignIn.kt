package com.ampersand.ampersandcontact.model


import com.google.gson.annotations.SerializedName

data class SignIn(
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String
)