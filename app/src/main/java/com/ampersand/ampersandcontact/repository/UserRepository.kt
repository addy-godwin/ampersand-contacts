package com.ampersand.ampersandcontact.repository

import android.content.Context
import com.ampersand.ampersandcontact.model.*
import com.ampersand.ampersandcontact.services.ContactsService
import com.ampersand.ampersandcontact.utils.getSavedUser
import com.ampersand.ampersandcontact.utils.saveUser
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class UserRepository(private val context: Context) {

    fun registerNewUser(
        newUser: Register,
        onSuccess: (Result.Success<UserToken>) -> Unit,
        onFailure: (Result.Error) -> Unit
    ): Disposable {

        return ContactsService
            .createService(null, context)
            .registerNewUser(newUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess(Result.Success(it))
            }, {
                if (it is HttpException) {
                    onFailure(
                        Result.Error(
                            ApiError(error = it.message(), code = it.code())
                        )
                    )
                }
            })
    }

    fun getCurrentUser(): UserToken? {
        return getSavedUser(context)
    }

    fun saveCurrentUser(userToken: UserToken) {
        saveUser(context, userToken)
    }

    fun signInUser(
        user: SignIn,
        onSuccess: (Result.Success<UserToken>) -> Unit,
        onFailure: (Result.Error) -> Unit
    ): Disposable {

        return ContactsService
            .createService(null, context)
            .login(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess(Result.Success(it))
            }, {
                if (it is HttpException) {
                    onFailure(
                        Result.Error(
                            ApiError(error = it.message(), code = it.code())
                        )
                    )
                }
            })
    }

    fun getUserProfile(
        userId: String, token: String,
        onSuccess: (Result.Success<User>) -> Unit,
        onFailure: (Result.Error) -> Unit
    ): Disposable {
        return ContactsService.createService(token, context)
            .getUserProfile(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess(Result.Success(it))
            }, {
                if (it is HttpException) {
                    onFailure(
                        Result.Error(
                            ApiError(error = it.message(), code = it.code())
                        )
                    )
                }

                onFailure(
                    Result.Error(
                        ApiError(error = "Timeout")
                    )
                )

            })
    }
}