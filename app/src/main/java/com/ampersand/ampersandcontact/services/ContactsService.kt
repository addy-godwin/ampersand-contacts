package com.ampersand.ampersandcontact.services

import android.content.Context
import com.ampersand.ampersandcontact.model.SignIn
import com.ampersand.ampersandcontact.model.Register
import com.ampersand.ampersandcontact.model.User
import com.ampersand.ampersandcontact.model.UserToken
import com.ampersand.ampersandcontact.utils.TIMEOUT_IN_SECONDS
import com.ampersand.ampersandcontact.utils.isOnline
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.Date
import java.util.concurrent.TimeUnit


interface ContactsService {

    @POST("register")
    fun registerNewUser(@Body newUser: Register?): Observable<UserToken>

    @POST("login")
    fun login(@Body signInUser: SignIn): Observable<UserToken>

    @POST("profile/{userId}")
    fun getUserProfile(@Path("userId") userId: String): Observable<User>


    companion object {

        private const val BASE_URL = "https://ampersand-contact-exchange-api.herokuapp.com/api/v1/"

        private val gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, DateDeserializer())
            .create()

        private val builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)


        fun createService(authToken: String?, context: Context): ContactsService {

            val httpClient = OkHttpClient.Builder()
                .addInterceptor(object : NetworkConnectionInterceptor() {
                    override fun isInternetAvailable(): Boolean {
                        return isOnline(context)
                    }

                    override fun onInternetUnavailable() {
                        if (context is InternetConnectionListener) {
                            context.onInternetUnavailable()
                        }
                    }
                })
                .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)


            if (authToken != null) {
                httpClient.addInterceptor { chain ->
                    val original = chain.request()

                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                        .header("x-access-token", authToken)
                        .method(original.method(), original.body())

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }
            }

            val client = httpClient.build()
            val retrofit = builder.client(client).build()
            return retrofit.create(ContactsService::class.java)
        }
    }

}