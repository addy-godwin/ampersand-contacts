package com.ampersand.ampersandcontact.controller.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.afterTextChanged
import com.ampersand.ampersandcontact.viewmodel.signin.SignInData
import com.ampersand.ampersandcontact.viewmodel.signin.SignInViewModel
import com.ampersand.ampersandcontact.viewmodel.signin.SignInViewModelFactory
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : BaseActivity() {

    private lateinit var signInViewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_signin)
        setTitle(R.string.sign_in)

        super.onCreate(savedInstanceState)

        signInViewModel = ViewModelProviders.of(this, SignInViewModelFactory(this)).get(SignInViewModel::class.java)

        initViews()
        observeViewModel()
    }

    private fun initViews() {
        emailInput.afterTextChanged { signInViewModel.dataChanged(getSignInData()) }
        passwordInput.afterTextChanged { signInViewModel.dataChanged(getSignInData()) }

        signInBtn.setOnClickListener {
            startSignIn()
        }
    }

    private fun observeViewModel() {
        signInViewModel.sigInFormState.observe(this,
            Observer {
                val signInFormState = it ?: return@Observer

                if (signInFormState.emailError != null) {
                    emailInput.error = ""
                    errorText.text = getString(signInFormState.emailError!!)
                }

                if (signInFormState.passwordError != null) {
                    passwordInput.error = ""
                    errorText.text = getString(signInFormState.passwordError!!)
                }

                signInBtn.isEnabled = signInFormState.isDataValid
            }
        )

        signInViewModel.userResultState.observe(this,
            Observer {
                val userResult = it ?: return@Observer

                loadingView.visibility = View.GONE
                if (userResult.success != null) {
                    signInViewModel.saveUser(userResult.success!!)

                    val i = Intent(this@SignInActivity, HomeActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(i)
                    finish()
                }

                if (userResult.apiError != null) {
                    val apiError = userResult.apiError!!

                    if (apiError.code == 403 || apiError.code == 422) {
                        errorText.text = apiError.error
                        return@Observer
                    }

                    showErrorDialog()
                }
            }
        )

    }

    private fun getSignInData(): SignInData {
        return SignInData(
            email = emailInput.text.toString(),
            password = passwordInput.text.toString()
        )
    }

    private fun showErrorDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.register_error)
            .setMessage(R.string.error_occurred)
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.setPositiveButton(R.string.try_again) { dialog, _ ->
                dialog.cancel()
                startSignIn()
            }
    }

    private fun startSignIn() {
        loadingView.visibility = View.VISIBLE
        signInViewModel.signInUser(getSignInData())
    }
}
