package com.ampersand.ampersandcontact.controller.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.LOG_KEY
import com.ampersand.ampersandcontact.utils.getSavedUser
import io.reactivex.plugins.RxJavaPlugins

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            if(getSavedUser(this) == null) {
                startGetStartedActivity()
                finish()
                return@postDelayed
            }

            startHomeActivity()
            finish()

        }, 1000L)

        RxJavaPlugins.setErrorHandler{
            Log.e(LOG_KEY, it.message)
        }
    }

    private fun startHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    private fun startGetStartedActivity() {
        startActivity(Intent(this, GetStartedActivity::class.java))
    }
}
