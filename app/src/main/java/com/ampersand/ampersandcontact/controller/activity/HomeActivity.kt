package com.ampersand.ampersandcontact.controller.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.makeQRCode
import com.ampersand.ampersandcontact.viewmodel.usertoken.UserTokenViewModel
import com.ampersand.ampersandcontact.viewmodel.usertoken.UserTokenViewModelFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private lateinit var userTokenViewModel: UserTokenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        userTokenViewModel = ViewModelProviders.of(
            this,
            UserTokenViewModelFactory(this)
        ).get(UserTokenViewModel::class.java)

        initViews()
        observeViewModel()
    }

    private fun initViews() {
        myProfileBtn.setOnClickListener {
            startActivity(Intent(this, MyProfileActivity::class.java))
        }

        scanQrBtn.setOnClickListener {
            startActivity(Intent(this, QRActivity::class.java))
        }
    }

    private fun observeViewModel() {
        userTokenViewModel.userTokenState.observe(this, Observer {
            val userToken = it ?: return@Observer
            val user = userToken.user

            userNameTxt.text = getString(
                R.string.full_name_template,
                user.firstName.capitalize(), user.lastName.capitalize()
            )

            Glide.with(this@HomeActivity)
//                .load("https://images.askmen.com/1080x540/2016/01/25-021526-facebook_profile_picture_affects_chances_of_getting_hired.jpg")
                .load(user.photo)
                .circleCrop()
                .into(profilePhotoThumb)


            Glide.with(this@HomeActivity)
                .load(makeQRCode(user.id, 270, 270))
                .fitCenter()
                .into(qrHolder)
        })
    }

}