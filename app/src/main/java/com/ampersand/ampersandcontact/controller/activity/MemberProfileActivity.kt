package com.ampersand.ampersandcontact.controller.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.EXTRA_QR_USER_ID
import com.ampersand.ampersandcontact.viewmodel.profile.UserProfileViewModel
import com.ampersand.ampersandcontact.viewmodel.profile.UserProfileViewModelFactory
import com.ampersand.ampersandcontact.viewmodel.usertoken.UserTokenViewModelFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_my_profile.*

class MemberProfileActivity : BaseActivity() {

    private lateinit var userProfileViewModel: UserProfileViewModel
    private lateinit var userId: String
    private var hasUser = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_my_profile)
        setTitle(R.string.meber_profile)

        super.onCreate(savedInstanceState)


        userProfileViewModel =
            ViewModelProviders.of(this, UserProfileViewModelFactory(this)).get(UserProfileViewModel::class.java)

        initViews()
        observeViewModel()
    }

    private fun initViews() {
        userId = intent.getStringExtra(EXTRA_QR_USER_ID)
    }

    private fun observeViewModel() {
        userProfileViewModel.userTokenState.observe(this,
            Observer {
                val userToken = it ?: return@Observer

                val token = userToken.token

                if(!hasUser) userProfileViewModel.findUser(userId, token)
            })

        userProfileViewModel.userState.observe(this,
            Observer {
                val result = it ?: return@Observer

                profileLoader.visibility = View.GONE
                profileInfoLayout.visibility = View.VISIBLE
                hasUser = true

                if (result.user != null) {
                    val userProfile = result.user

                    userNameTxt.text = getString(
                        R.string.full_name_template,
                        userProfile.firstName.capitalize(), userProfile.lastName.capitalize()
                    )
                    phoneTxt.text = userProfile.phoneNumber
                    emailTxt.text = emailTxt.text

                    Glide.with(this)
                        .load(userProfile.photo)
                        .into(profilePhotoView)

                    profileLoader.visibility = View.GONE
                    profileInfoLayout.visibility = View.VISIBLE
                }

                if (result.apiError != null) {
                    Toast.makeText(this@MemberProfileActivity, result.apiError.error, Toast.LENGTH_LONG).show()
                    finish()
                }
            })
    }
}
