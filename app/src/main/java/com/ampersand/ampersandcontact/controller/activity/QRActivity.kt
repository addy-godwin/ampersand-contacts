package com.ampersand.ampersandcontact.controller.activity

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.SurfaceHolder
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.EXTRA_QR_USER_ID
import com.ampersand.ampersandcontact.utils.LOG_KEY
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_qr_scan.*

class QRActivity : AppCompatActivity() {

    private var cameraCaptureSession: CameraCaptureSession? = null
    private var imgReader: ImageReader? = null
    private var cameraImage: Image? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_qr_scan)

        checkPermissions()
        initViews()
    }

    private fun initViews() {
        closeBtn.setOnClickListener {
            cleanUp()
            finish()
        }
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.VIBRATE
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                        if (report.areAllPermissionsGranted()) {
                            qrScannerWindow.holder.addCallback(object : SurfaceHolder.Callback {
                                override fun surfaceChanged(
                                    holder: SurfaceHolder?,
                                    format: Int,
                                    width: Int,
                                    height: Int
                                ) {
                                    startCameraPreview(width, height)
                                }

                                override fun surfaceDestroyed(holder: SurfaceHolder?) {

                                }

                                override fun surfaceCreated(holder: SurfaceHolder?) {

                                }
                            })
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            finish()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                    }
                })
                .onSameThread()
                .check()
        }
    }

    @TargetApi(21)
    private fun startCameraPreview(width: Int, height: Int) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            showUnsupportedDialog()
        }

        try {
            val cameraBackgroundHandler = Handler()

            val cameraManager = this.getSystemService(Context.CAMERA_SERVICE) as CameraManager

            cameraManager.cameraIdList.find {
                val cameraPosition = cameraManager.getCameraCharacteristics(it).get(CameraCharacteristics.LENS_FACING)

                return@find cameraPosition != null && cameraPosition == CameraCharacteristics.LENS_FACING_BACK
            }?.let {

                val cameraStateCallback = object : CameraDevice.StateCallback() {

                    override fun onOpened(camera: CameraDevice) {
                        val barcodeDetector = BarcodeDetector
                            .Builder(this@QRActivity)
                            .setBarcodeFormats(Barcode.QR_CODE)
                            .build()

                        if (!barcodeDetector.isOperational) {
                        }

                        imgReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1)

                        imgReader!!.setOnImageAvailableListener({ imageReader ->
                            cameraImage = imageReader.acquireNextImage()
                            val buffer = cameraImage!!.planes.first().buffer
                            val imageBytes = ByteArray(buffer.capacity())
                            buffer.get(imageBytes)

                            val bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.count(), null)
                            val frameToProcess = Frame.Builder().setBitmap(bitmap).build()
                            val barCodeResults = barcodeDetector.detect(frameToProcess)

                            if (barCodeResults.size() != 0) {
                                cleanUp()
                                val code = barCodeResults.valueAt(0).displayValue
                                showUserProfile(code)
                            }

                            cameraImage?.close()
                        }, cameraBackgroundHandler)

                        val captureStateCallback = object : CameraCaptureSession.StateCallback() {

                            override fun onConfigured(session: CameraCaptureSession) {
                                cameraCaptureSession = session
                                val builder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
                                builder.addTarget(qrScannerWindow.holder.surface)
                                builder.addTarget(imgReader!!.surface)
                                session.setRepeatingRequest(builder.build(), null, null)
                            }

                            override fun onConfigureFailed(session: CameraCaptureSession) {
                                Log.d(LOG_KEY, "camera configuration failed")
                            }
                        }

                        camera.createCaptureSession(
                            listOf(qrScannerWindow.holder.surface, imgReader!!.surface),
                            captureStateCallback,
                            cameraBackgroundHandler
                        )
                    }

                    override fun onDisconnected(camera: CameraDevice) {
                        Log.d(LOG_KEY, "camera disconnected")
                    }

                    override fun onError(camera: CameraDevice, error: Int) {
                        Log.d(LOG_KEY, "camera error")
                    }
                }

                cameraManager.openCamera(it, cameraStateCallback, cameraBackgroundHandler)
                return
            }

            showUnsupportedDialog()

        } catch (e: CameraAccessException) {
            Log.d(LOG_KEY, "camera access exception")
        } catch (e: SecurityException) {
            Log.d(LOG_KEY, "security exception")
        }
    }

    private fun showUserProfile(code: String) {
        val i = Intent(this, MemberProfileActivity::class.java)
        i.putExtra(EXTRA_QR_USER_ID, code)
        startActivity(i)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        cleanUp()
    }

    private fun showUnsupportedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Unsupported Device")
            .setMessage("Sorry, You device does not support this feature")
            .setPositiveButton("OK") { dialog, _ ->
                dialog.cancel()
                finish()
            }
            .setCancelable(false)
            .create().show()
    }

    private fun cleanUp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cameraImage?.close()
            cameraCaptureSession?.abortCaptures()
            imgReader?.close()
        }
    }
}