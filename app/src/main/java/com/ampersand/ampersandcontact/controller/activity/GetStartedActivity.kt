package com.ampersand.ampersandcontact.controller.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ampersand.ampersandcontact.R
import kotlinx.android.synthetic.main.activity_get_started.*

class GetStartedActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)

        getStartedBtn.setOnClickListener {
            startActivity(Intent(this, StartupActivity::class.java))
        }
    }
}