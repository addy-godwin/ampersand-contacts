package com.ampersand.ampersandcontact.controller.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.ampersand.ampersandcontact.R

class StartupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)
    }

    fun gotoSignIn(v: View) {
        startActivity(Intent(this, SignInActivity::class.java))
    }

    fun gotoRegister(v: View){
        startActivity(Intent(this, RegisterActivity::class.java))
    }
}
