package com.ampersand.ampersandcontact.controller.activity

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.utils.CAMERA_REQUEST_CODE
import com.ampersand.ampersandcontact.utils.GALLERY_REQUEST_CODE
import com.ampersand.ampersandcontact.utils.afterTextChanged
import com.ampersand.ampersandcontact.viewmodel.register.RegisterData
import com.ampersand.ampersandcontact.viewmodel.register.RegisterViewModel
import com.ampersand.ampersandcontact.viewmodel.register.RegisterViewModelFactory
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.dialog_select_photo.view.*
import java.io.File

class RegisterActivity : BaseActivity(), View.OnFocusChangeListener {

    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_register)
        setTitle(R.string.register)

        super.onCreate(savedInstanceState)

        registerViewModel = ViewModelProviders
            .of(this, RegisterViewModelFactory(this))
            .get(RegisterViewModel::class.java)

        initViews()
        observeViewModel()
    }

    private fun initViews() {
        fullNameInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        emailInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        passwordInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        phoneInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        twitterInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        linkedInInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }
        websiteInput.afterTextChanged { registerViewModel.dataChanged(getRegisterData()) }

        registerBtn.setOnClickListener {
            startRegister()
        }

        profilePhotoView.setOnClickListener { checkPermissions() }
    }

    private fun observeViewModel() {
        registerViewModel.registerFormState
            .observe(this@RegisterActivity,
                Observer {
                    val formState = it ?: return@Observer

                    if (formState.profilePhoto != null) {
                        Glide.with(this@RegisterActivity)
                            .load(formState.profilePhoto)
                            .into(profilePhotoView)

                        enableEditButton()
                    }

                    if (formState.fullNameError != null) {
                        fullNameInput.error = ""
                        errorText.text = getString(formState.fullNameError!!)
                    }

                    if (formState.emailError != null) {
                        emailInput.error = ""
                        errorText.text = getString(formState.emailError!!)
                    }

                    if (formState.passwordError != null) {
                        passwordInput.error = ""
                        errorText.text = getString(formState.passwordError!!)
                    }

                    if (formState.phoneNumberError != null) {
                        phoneInput.error = ""
                        errorText.text = getString(formState.phoneNumberError!!)
                    }

                    registerBtn.isEnabled = formState.isDataValid
                })

        registerViewModel.userResultState
            .observe(this@RegisterActivity,
                Observer {
                    val registerResult = it ?: return@Observer

                    loadingView.visibility = View.GONE

                    if (registerResult.success != null) {
                        registerViewModel.saveUser()
                        val i = Intent(this@RegisterActivity, HomeActivity::class.java)
                        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(i)
                        finish()
                    }

                    if (registerResult.apiError != null) {
                        val apiError = registerResult.apiError!!

                        if (apiError.code == 403 || apiError.code == 422) {
                            errorText.text = apiError.error
                            return@Observer
                        }

                        showErrorDialog()
                    }
                })
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        registerViewModel.dataChanged(getRegisterData())
    }

    private fun getRegisterData(): RegisterData {
        return RegisterData(
            email = emailInput.text.toString(),
            fullName = fullNameInput.text.toString(),
            password = passwordInput.text.toString(),
            phoneNumber = phoneInput.text.toString(),
            twitter = twitterInput.text.toString(),
            linkedIn = linkedInInput.text.toString(),
            website = websiteInput.text.toString()
        )
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.INTERNET
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                        if (report.areAllPermissionsGranted()) {
                            pickProfilePhotoDialog()
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                    }
                })
                .onSameThread()
                .check()
        }
    }

    private fun pickProfilePhotoDialog() {

        val view = layoutInflater.inflate(R.layout.dialog_select_photo, null)

        val dialog = AlertDialog.Builder(this)
            .setTitle(getString(R.string.select_profile_photo))
            .setView(view)
            .create()

        dialog.show()

        view.takePictureBtn.setOnClickListener {
            dialog.cancel()
            pickFromCamera()
        }
        view.selectFromGalleryBtn.setOnClickListener {
            dialog.cancel()
            pickFromGallery()
        }
        view.removePhotoBtn.setOnClickListener {
            dialog.cancel()
            disableEditButton()
        }
    }

    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/jpeg", "image/png"))
        }

        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun pickFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photoFileName = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "IMG_PP.jpg")

        if (intent.resolveActivity(packageManager) != null) {
            val currentImageUri = Uri.fromFile(photoFileName)

            intent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri)

            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())

            startActivityForResult(intent, CAMERA_REQUEST_CODE)
        }
    }

    private fun enableEditButton() {
        editPhotoBtn.visibility = View.VISIBLE
        editPhotoBtn.setOnClickListener { pickProfilePhotoDialog() }
        profilePhotoView.setOnClickListener {}
    }

    private fun disableEditButton() {
        Glide.with(this@RegisterActivity)
            .load(R.drawable.profile_placeholder)
            .into(profilePhotoView)

        editPhotoBtn.visibility = View.GONE
        editPhotoBtn.setOnClickListener { }
        profilePhotoView.setOnClickListener { pickProfilePhotoDialog() }
        registerViewModel.updateProfilePhotoData(null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                GALLERY_REQUEST_CODE -> {
                    val currentImageUri = data?.data
                    val photoBitmap = getPhotoBitmap(currentImageUri)

                    enableEditButton()
                    registerViewModel.updateProfilePhotoData(photoBitmap)
                }

                CAMERA_REQUEST_CODE -> {
                    val photoFileName = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "IMG_PP.jpg")
                    val currentImageUri = Uri.fromFile(photoFileName)
                    val photoBitmap = getPhotoBitmap(currentImageUri)

                    enableEditButton()
                    registerViewModel.updateProfilePhotoData(photoBitmap)
                }
            }
        }

        if (resultCode == Activity.RESULT_CANCELED
            && requestCode == CAMERA_REQUEST_CODE
        ) {
            disableEditButton()
        }
    }

    private fun getPhotoBitmap(uri: Uri?) = MediaStore.Images.Media.getBitmap(contentResolver, uri)

    private fun showErrorDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.register_error)
            .setMessage(R.string.error_occurred)
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.setPositiveButton(R.string.try_again) { dialog, _ ->
                dialog.cancel()
                startRegister()
            }
    }

    private fun startRegister() {
        loadingView.visibility = View.VISIBLE
        registerViewModel.registerUser(getRegisterData())
    }

}
