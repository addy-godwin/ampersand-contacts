package com.ampersand.ampersandcontact.controller.activity

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.ampersand.ampersandcontact.R
import kotlinx.android.synthetic.main.activity_base.*

open class BaseActivity: AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)

        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_base)
        layoutInflater.inflate(layoutResID, baseLayout)
    }

    override fun setTitle(@StringRes titleId: Int) {
        super.setTitle(titleId)
        toolbarTitle.text = resources.getString(titleId)
    }
}