package com.ampersand.ampersandcontact.controller.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ampersand.ampersandcontact.R
import com.ampersand.ampersandcontact.viewmodel.usertoken.UserTokenViewModel
import com.ampersand.ampersandcontact.viewmodel.usertoken.UserTokenViewModelFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_my_profile.*

class MyProfileActivity : BaseActivity() {

    private lateinit var userTokenViewModel: UserTokenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_my_profile)
        setTitle(R.string.my_profile)

        super.onCreate(savedInstanceState)

        userTokenViewModel = ViewModelProviders.of(this, UserTokenViewModelFactory(this)).get(UserTokenViewModel::class.java)

//        initViews()
        observeViewModel()
    }

    private fun observeViewModel() {
        userTokenViewModel.userTokenState.observe(this,
            Observer {
                val userToken = it ?: return@Observer

                val user = userToken.user

                fullNameTxt.text = getString(
                    R.string.full_name_template,
                    user.firstName.capitalize(), user.lastName.capitalize()
                )

                phoneTxt.text = user.phoneNumber
                emailTxt.text = user.email

                println("Photo Url ${user.photo}")

                Glide.with(this)
                    .load(user.photo)
                    .into(profilePhotoView)

                profileLoader.visibility = View.GONE
                profileInfoLayout.visibility = View.VISIBLE
            })
    }

}